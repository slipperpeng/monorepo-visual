import { DEFAULT_LAYOUT } from '../base';
import { AppRouteRecordRaw } from '../types';

const AUTOMATEDBUILD: AppRouteRecordRaw = {
  path: '/automatedBuild',
  name: 'automatedBuild',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '自动化构建',
    requiresAuth: true,
    icon: 'icon-dashboard',
    order: 0,
  },
  children: [
    {
      path: 'automatedBuild',
      name: 'AutomatedBuild',
      component: () => import('@/views/automatedBuild/index.vue'),
      meta: {
        locale: '工作台',
        requiresAuth: true,
        roles: ['admin'],
      },
    },
  ],
};

export default AUTOMATEDBUILD;
