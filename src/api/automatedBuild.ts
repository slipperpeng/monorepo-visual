/* eslint-disable no-use-before-define */
import axios from 'axios';
import type { AxiosRequestConfig, AxiosResponse } from 'axios';
import { HttpResponse } from './interceptor';

const API = axios.create({
  baseURL: 'https://gitlab.com/',
});

API.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    if (!config.headers) {
      config.headers = {};
    }
    const accessToken = 'glpat-WTUoZxjsLsLYEWbvsTQMS';
    config.headers.Authorization = `Bearer ${accessToken}`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

API.interceptors.response.use(
  (response: AxiosResponse<HttpResponse>) => {
    const res = response.data;
    return res;
  },
  (error) => {
    return Promise.reject(error);
  }
);

/** 获取该用户所拥有的所有项目信息 */
export function getAllProjects(): Promise<AllProjectItem[]> {
  return API.get(`api/v4/projects?owned=true`);
}

/** 获取项目中仓库文件和目录的列表 */
export function getRepositoryTree(id: number): Promise<RepositoryTreeItem[]> {
  return API.get(`api/v4/projects/${id}/repository/tree?path=apps`);
}

/** 获取一个项目的按分支名的字母顺序排列的仓库分支列表 */
export function getRepositoryBranches(
  id: number
): Promise<RepositoryBranchesItem[]> {
  return API.get(`api/v4/projects/${id}/repository/branches`);
}

interface Projectaccess {
  access_level: number;
  notification_level: number;
}

interface Permissions {
  project_access: Projectaccess;
  group_access?: any;
}

interface Containerexpirationpolicy {
  cadence: string;
  enabled: boolean;
  keep_n: number;
  older_than: string;
  name_regex: string;
  name_regex_keep?: any;
  next_run_at: string;
}

interface Owner {
  id: number;
  username: string;
  name: string;
  state: string;
  avatar_url: string;
  web_url: string;
}

interface Links {
  self: string;
  issues: string;
  merge_requests: string;
  repo_branches: string;
  labels: string;
  events: string;
  members: string;
  cluster_agents: string;
}

interface Namespace {
  id: number;
  name: string;
  path: string;
  kind: string;
  full_path: string;
  parent_id?: any;
  avatar_url: string;
  web_url: string;
}

export interface AllProjectItem {
  id: number;
  description?: any;
  name: string;
  name_with_namespace: string;
  path: string;
  path_with_namespace: string;
  created_at: string;
  default_branch: string;
  tag_list: any[];
  topics: any[];
  ssh_url_to_repo: string;
  http_url_to_repo: string;
  web_url: string;
  readme_url?: any;
  avatar_url?: any;
  forks_count: number;
  star_count: number;
  last_activity_at: string;
  namespace: Namespace;
  container_registry_image_prefix: string;
  _links: Links;
  packages_enabled: boolean;
  empty_repo: boolean;
  archived: boolean;
  visibility: string;
  owner: Owner;
  resolve_outdated_diff_discussions: boolean;
  container_expiration_policy: Containerexpirationpolicy;
  issues_enabled: boolean;
  merge_requests_enabled: boolean;
  wiki_enabled: boolean;
  jobs_enabled: boolean;
  snippets_enabled: boolean;
  container_registry_enabled: boolean;
  service_desk_enabled: boolean;
  service_desk_address: string;
  can_create_merge_request_in: boolean;
  issues_access_level: string;
  repository_access_level: string;
  merge_requests_access_level: string;
  forking_access_level: string;
  wiki_access_level: string;
  builds_access_level: string;
  snippets_access_level: string;
  pages_access_level: string;
  operations_access_level: string;
  analytics_access_level: string;
  container_registry_access_level: string;
  security_and_compliance_access_level: string;
  releases_access_level: string;
  environments_access_level: string;
  feature_flags_access_level: string;
  infrastructure_access_level: string;
  monitor_access_level: string;
  emails_disabled: boolean;
  shared_runners_enabled: boolean;
  lfs_enabled: boolean;
  creator_id: number;
  import_url?: any;
  import_type?: any;
  import_status: string;
  open_issues_count: number;
  ci_default_git_depth: number;
  ci_forward_deployment_enabled: boolean;
  ci_job_token_scope_enabled: boolean;
  ci_separated_caches: boolean;
  ci_opt_in_jwt: boolean;
  ci_allow_fork_pipelines_to_run_in_parent_project: boolean;
  public_jobs: boolean;
  build_timeout: number;
  auto_cancel_pending_pipelines: string;
  ci_config_path: string;
  shared_with_groups: any[];
  only_allow_merge_if_pipeline_succeeds: boolean;
  allow_merge_on_skipped_pipeline?: any;
  restrict_user_defined_variables: boolean;
  request_access_enabled: boolean;
  only_allow_merge_if_all_discussions_are_resolved: boolean;
  remove_source_branch_after_merge: boolean;
  printing_merge_request_link_enabled: boolean;
  merge_method: string;
  squash_option: string;
  enforce_auth_checks_on_uploads: boolean;
  suggestion_commit_message?: any;
  merge_commit_template?: any;
  squash_commit_template?: any;
  issue_branch_template?: any;
  auto_devops_enabled: boolean;
  auto_devops_deploy_strategy: string;
  autoclose_referenced_issues: boolean;
  keep_latest_artifact: boolean;
  runner_token_expiration_interval?: any;
  external_authorization_classification_label: string;
  requirements_enabled: boolean;
  requirements_access_level: string;
  security_and_compliance_enabled: boolean;
  compliance_frameworks: any[];
  permissions: Permissions;
}

interface RepositoryTreeItem {
  id: string;
  name: string;
  type: string;
  path: string;
  mode: string;
}

interface RepositoryBranchesItem {
  name: string;
  commit: Commit;
  merged: boolean;
  protected: boolean;
  developers_can_push: boolean;
  developers_can_merge: boolean;
  can_push: boolean;
  default: boolean;
  web_url: string;
}

export interface Commit {
  id: string;
  short_id: string;
  created_at: string;
  parent_ids: any[];
  title: string;
  message: string;
  author_name: string;
  author_email: string;
  authored_date: string;
  committer_name: string;
  committer_email: string;
  committed_date: string;
  trailers: Record<string, unknown>;
  web_url: string;
}
