/* eslint-disable import/prefer-default-export */

import axios, { AxiosResponse } from 'axios';
import { Commit } from './automatedBuild';
import { HttpResponse } from './interceptor';

const API = axios.create({
  baseURL: '/server',
});
API.interceptors.response.use(
  (response: AxiosResponse<HttpResponse>) => {
    const res = response.data;
    return res;
  },
  (error) => {
    return Promise.reject(error);
  }
);

/** 开始构建 */
export function startBuildAPI(params: {
  currentSelectAppList: string[];
  currentSelectbranch: {
    name?: string | undefined;
    commit?: Commit | undefined;
  };
}): Promise<void> {
  return API.post(`/startBuild`, params);
}
